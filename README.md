**# Test task for On Air entertainment

During this test task I was trying to build actor based system, using Akka with Event source and CQRS behaviour.

I have experience with classic actor systems (2.5, 2.6), but was trying to refresh and get knowledge about typed actor
system

# Architectural preview

![Overview](docs/overview.png "Overview")

This solution contains 2 projects: 1 is core, where actor system, event sourced behaviour and CQRS must live, and
another, is REST API endpoint, where we will open our application to world.
No authorisation or authentication was implemented yet.
Rest API I planned to be like this:

| verb  | path                        | description                                                                  |
|-------|-----------------------------|------------------------------------------------------------------------------|
| GET   | /api/random                 | - returns random number (singleton which calls default scala implementation) |
| GET   | /api/games?startAt=x&take=y | - paginated collection of all games which we have in DB                      |
| GET   | /api/games/:id              | - returns games by its id                                                    |
| GET   | /api/games/:id/replay       | - replay game in same room                                                   |
| POST  | /api/games                  | - creates new game                                                           | 
| POST  | /api/games/:id/players      | - adds new player to game                                                    |
| GET   | /api/games/:id/players/:id  | - returns player and his HAND in game                                        |
| GET   | /api/ws                     | - open new websocket connection                                              |

Currently, I've implemented only getting random number and creating new game. For proper handling of GET requests on
game, actor system don't have yet indexes.

During implementation, I planned to use Akka persistence, for event sourced behaviour with jdbc plugin. Like it was in
classic actor system. But I understood, that probably I should use akka projection to create CQRS in Typed systems
properly. This waht I plan to do next.
Websockets are also only in plans.

On all this work I spent near 12-13 hours. TBH, I assume with classic, it maybe some hours less.

For tests of core part, it's easy and done with defaults right now. For http part, I'm not yet finished with core, so
cannot predict what will be changed there.

For DB, I used postgres, it has nice indices, and I'm not sure if it's ready for high load. For better application
performance, I would rather switch to Cassandra as Event Store and elasticsearch as CQRS database.

Also, I created Cluster and shards of game rooms on it. It was done for scalability, which application will have only in
the end. Need also to add discovery service to join from other instance to this actor system, but that maybe done only
later, when application will have its executables and at least docker compose file, where I'll be able to start several
instances for core application.

# Conclusion

I see a lot of time was spent on more technical issues, settings, but task is not finished. Event if you will have
negative
decision after review, thanks for task to refresh knowledge. But I would also appreciate for a feedback, with my
mistakes.**