package com.onairentertainment.base

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.onairentertainment.core.CoreServices.system
import com.onairentertainment.core.services.{GamesService, GamesServiceImpl}
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfter, Suite}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

trait BaseRoutesTest
    extends AnyWordSpecLike
    with BeforeAndAfter
    with Matchers
    with ScalaFutures
    with ScalatestRouteTest {

  lazy val testKit: ActorTestKit = ActorTestKit(ConfigFactory.parseString("""
      akka.actor.provider="cluster"
    """))

  override def createActorSystem(): akka.actor.ActorSystem =
    testKit.system.classicSystem

}
