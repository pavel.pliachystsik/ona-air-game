package com.onairentertainment.base

import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpMethods,
  HttpRequest
}
import spray.json.JsValue

object TestClient {

  def get(url: String): HttpRequest = HttpRequest(
    method = HttpMethods.GET,
    uri = url
  )

  def post(url: String, body: JsValue): HttpRequest = HttpRequest(
    method = HttpMethods.POST,
    uri = url,
    entity = HttpEntity.apply(ContentTypes.`application/json`, body.toString)
  )

}
