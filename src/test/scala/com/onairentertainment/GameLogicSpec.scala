package com.onairentertainment

import akka.http.scaladsl.model.{HttpMethods, HttpRequest, StatusCodes}
import com.onairentertainment.base.{BaseRoutesTest, TestClient}
import com.onairentertainment.model.requests.NewGameRequest
import com.onairentertainment.model.responses.{
  GameResponse,
  RandomNumberResponse
}
import spray.json._

import com.onairentertainment.routes.{GamesRoutes, RandomRoutes}

class GameLogicSpec extends BaseRoutesTest {

  "GameLogic" must {
    "random number route" in {
      TestClient.get("/random") ~!> RandomRoutes.route ~> check {
        status mustBe StatusCodes.OK
        responseAs[RandomNumberResponse]
      }
    }
  }

}
