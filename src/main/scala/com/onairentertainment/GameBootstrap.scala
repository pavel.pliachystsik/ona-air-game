package com.onairentertainment

import akka.actor.typed.ActorSystem
import akka.cluster.typed.Cluster
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.onairentertainment.core.CoreServices
import com.onairentertainment.routes.{GamesRoutes, RandomRoutes}
import com.typesafe.config.ConfigFactory

import scala.concurrent.ExecutionContextExecutor
import scala.io.StdIn

object GameBootstrap extends App {

  println("Server started")

  val config = ConfigFactory.load()
  val host = config.getString("http.host")
  val port = config.getInt("http.port")

  implicit val system: ActorSystem[Nothing] =
    CoreServices.system

  implicit val ec: ExecutionContextExecutor = system.executionContext

  val restApiRoutes: Route = pathPrefix("api") {
    concat(
      RandomRoutes.route,
      GamesRoutes.route
    )
  }

  val binding = Http().newServerAt(host, port).bind(restApiRoutes)

  binding.failed.foreach { ex =>
    system.log.error(s"Failed to bind to $host:$port!")
    ex.printStackTrace()
  }

  StdIn.readLine()

  binding
    .flatMap(_.unbind())
    .onComplete { _ =>
      CoreServices.leaveCluster()
      system.terminate()
    }

}
