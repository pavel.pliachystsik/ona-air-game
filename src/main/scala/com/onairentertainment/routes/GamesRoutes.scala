package com.onairentertainment.routes

import akka.http.scaladsl.model.StatusCodes.{BadRequest, OK}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.onairentertainment.core.CoreException
import com.onairentertainment.model.requests.NewGameRequest
import com.onairentertainment.model.responses.GameResponse
import com.onairentertainment.core.CoreServices

import java.util.UUID
import scala.util.{Failure, Success}

object GamesRoutes extends BaseRoute {
  val domain = "games"

  override def route: Route = concat(
    path(domain)(gamesRoutes),
    pathPrefix(domain / JavaUUID)(gameRoute)
  )

  private lazy val gamesRoutes: Route = {
    concat(
      get {
        ???
      },
      post {
        decodeRequest {
          entity(as[NewGameRequest]) { newGameRequest =>
            onComplete(
              CoreServices.gamesService.createNewGame(newGameRequest.players)
            ) {
              case Success(value) =>
                complete(OK, GameResponse(value))
              case Failure(exception: CoreException) =>
                complete(BadRequest, exception.message)
            }
          }
        }
      }
    )
  }
  private def gameRoute(id: UUID): Route = {
    get {
      decodeRequest {
        onComplete(CoreServices.gamesService.getGame(id)) {
          case Success(value) =>
            complete(OK, GameResponse(value))
          case Failure(exception: CoreException) =>
            complete(BadRequest, exception.message)
        }
      }
    }
  }

}
