package com.onairentertainment.routes

import akka.http.scaladsl.server.Route

trait BaseRoute {
  def domain: String

  def route: Route
}
