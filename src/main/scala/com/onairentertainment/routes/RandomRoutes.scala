package com.onairentertainment.routes

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.onairentertainment.core.services.Randomizer
import com.onairentertainment.model.responses.RandomNumberResponse

object RandomRoutes extends BaseRoute {

  val domain = "random"

  override def route: Route = path(domain)(get {
    complete(RandomNumberResponse(Randomizer.getNext))
  })

}
