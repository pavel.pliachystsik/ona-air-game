package com.onairentertainment.model.requests

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.onairentertainment.core.persistence.game.behaviours.GameRoom
import spray.json.{DefaultJsonProtocol, RootJsonFormat}

case class NewGameRequest(players: Int)

object NewGameRequest extends SprayJsonSupport with DefaultJsonProtocol {

  implicit val newGameRequestFormat: RootJsonFormat[NewGameRequest] =
    jsonFormat1(NewGameRequest.apply)
}
