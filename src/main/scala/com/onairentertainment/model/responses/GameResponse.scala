package com.onairentertainment.model.responses

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.onairentertainment.core.persistence.game.behaviours.GameRoom
import spray.json.{
  DefaultJsonProtocol,
  JsNumber,
  JsObject,
  JsString,
  JsValue,
  RootJsonFormat
}
import com.onairentertainment.model.util.UUIDFormat

import java.util.UUID

case class GameResponse(id: UUID, players: Int) {
  def selfLink: String = s"/games/$id"
}

object GameResponse extends SprayJsonSupport with DefaultJsonProtocol {
  def apply(state: GameRoom.State): GameResponse =
    new GameResponse(state.id, state.players)

  implicit object GameResponseJsonFormat extends RootJsonFormat[GameResponse] {
    val defaultFormat: RootJsonFormat[GameResponse] = jsonFormat2(
      GameResponse.apply
    )

    def write(c: GameResponse): JsObject = JsObject(
      "id" -> JsString(c.id.toString),
      "players" -> JsNumber(c.players),
      "selfLink" -> JsString(c.selfLink)
    )

    override def read(json: JsValue): GameResponse = defaultFormat.read(json)
  }
}
