package com.onairentertainment.model.responses

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json._

case class RandomNumberResponse(value: Int) extends Response

object RandomNumberResponse extends SprayJsonSupport with DefaultJsonProtocol {

  implicit val randomNumberFormat: RootJsonFormat[RandomNumberResponse] =
    jsonFormat1(RandomNumberResponse.apply)

}
