package com.onairentertainment.core.indexes.models

import java.util.UUID

case class GameRoomIndexEntry(id: UUID, players: Int)
