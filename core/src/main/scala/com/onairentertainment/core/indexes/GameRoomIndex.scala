package com.onairentertainment.core.indexes
import akka.Done
import akka.actor.typed.ActorSystem
import akka.persistence.jdbc.query.scaladsl.JdbcReadJournal
import akka.persistence.query.PersistenceQuery
import akka.stream.{Materializer, SystemMaterializer}
import com.onairentertainment.core.persistence.game.behaviours.GameRoom
import com.onairentertainment.core.repository.HasDB
import slick.jdbc.JdbcBackend

import scala.concurrent.Future

class GameRoomIndexImpl(
    system: ActorSystem[_],
    override val db: JdbcBackend.Database
) extends HasDB {

  implicit val materializer: Materializer = SystemMaterializer(
    system
  ).materializer

  val readJournal: JdbcReadJournal =
    PersistenceQuery(system).readJournalFor[JdbcReadJournal](
      JdbcReadJournal.Identifier
    )

  val eventsStream = readJournal.eventsByTag("tag1", 0L).runForeach { event =>
    event.event match {
      // GameRoom Events
      case GameRoom.NewGameCreated(id, players) => Future.successful(Done)
      case _                                    => Future.successful(Done)
    }
  }

}

trait GameRoomIndex {
  def gameRoomIndex: GameRoomIndexImpl
}
