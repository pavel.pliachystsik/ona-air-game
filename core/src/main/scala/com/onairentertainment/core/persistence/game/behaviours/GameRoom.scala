package com.onairentertainment.core.persistence.game.behaviours

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.cluster.sharding.typed.scaladsl.EntityTypeKey
import akka.pattern.StatusReply
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.{Effect, EventSourcedBehavior}
import com.onairentertainment.core.{GameRules, JSONSerialization}
import com.onairentertainment.core.services.Randomizer

import java.util.UUID
import scala.util.{Failure, Success, Try}

object GameRoom {

  val typeKey: EntityTypeKey[Command] =
    EntityTypeKey[Command]("GameRoom")

  type Hand = (Int, Int)
  type Winner = Seq[Int]

  sealed trait Command {
    def replyTo: ActorRef[StatusReply[State]]

    def toEffect(state: State): Effect[Event, State]
  }

  final case class NewGame(
      players: Int,
      replyTo: ActorRef[StatusReply[State]]
  ) extends Command {
    def toEffect(state: State): Effect[Event, State] = {
      if (state.games.nonEmpty)
        Effect.none.thenRun(_ =>
          replyTo ! StatusReply.Error("GAME_ALREADY_CREATED")
        )
      else if (players < 2)
        Effect.none.thenRun(_ =>
          replyTo ! StatusReply.Error("NOT_ENOUGH_PLAYERS")
        )
      else
        Effect
          .persist(NewGameCreated(state.id, players))
          .thenRun(state => replyTo ! StatusReply.Success(state))
    }
  }
  final case class AddPlayer(
      player: Int,
      number: Int,
      replyTo: ActorRef[StatusReply[State]]
  ) extends Command {
    def toEffect(state: State): Effect[Event, State] = {
      val lastGame =
        if (state.games.isEmpty) Draw(Seq.empty)
        else state.games(state.games.length - 1)
      val isLastHand = lastGame.hands.size + 1 == state.players
      val playerPlayed = lastGame.hands.exists(_._1 == player)
      val playerExists = player < state.players
      if (!playerExists)
        Effect.none.thenRun(_ =>
          replyTo ! StatusReply.Error("PLAYER_NOT_EXISTS")
        )
      else if (playerPlayed)
        Effect.none.thenRun(_ => replyTo ! StatusReply.Error("PLAYER_PLAYED"))
      else if (isLastHand) {
        Try(GameRules.solve(state.games.last.hands :+ (player, number))) match {
          case Success(solution) =>
            val winners = solution
              .takeWhile(_._2 == solution.head._2)
              .map(_._1)
            Effect
              .persist(
                Seq(
                  PlayerAdded(state.id, player, number),
                  Solved(state.id, winners)
                )
              )
              .thenRun(state => replyTo ! StatusReply.Success(state))
          case Failure(exception) =>
            Effect.none.thenRun(_ =>
              replyTo ! StatusReply.Error(exception.getMessage)
            )
        }
      } else {
        Effect
          .persist(PlayerAdded(state.id, player, number))
          .thenRun(state => replyTo ! StatusReply.Success(state))
      }
    }
  }
  final case class Replay(replyTo: ActorRef[StatusReply[State]])
      extends Command {
    def toEffect(state: State): Effect[PlayerAdded, State] = {
      Effect
        .persist(
          (0 until state.players).map(playerId =>
            PlayerAdded(state.id, playerId, Randomizer.getNext)
          )
        )
        .thenRun(state => replyTo ! StatusReply.Success(state))
    }
  }

  sealed trait Event extends JSONSerialization {
    def id: UUID
  }
  final case class NewGameCreated(id: UUID, players: Int) extends Event
  final case class PlayerAdded(id: UUID, player: Int, number: Int) extends Event
  final case class Solved(id: UUID, winners: Seq[Int]) extends Event
  sealed trait Response
  final case class State(
      id: UUID,
      players: Int,
      games: Array[Draw]
  ) extends Response
      with JSONSerialization

  final case class Draw(hands: Seq[Hand], winner: Winner = Seq.empty)

  def apply(id: UUID, persistenceId: PersistenceId): Behavior[Command] =
    Behaviors.setup { context: ActorContext[Command] =>
      context.log.info("Starting GameRoom {}", id)

      EventSourcedBehavior[Command, Event, State](
        persistenceId = persistenceId,
        emptyState = State(id, 0, Array.empty),
        commandHandler = commandHandler(context),
        eventHandler = eventHandler
      ).withTagger(_ => Set("tag1"))
        .snapshotWhen((state, _, _) =>
          state.players > 0 && Randomizer.getNext / 10000 > 0
        )
    }

  val commandHandler
      : ActorContext[Command] => (State, Command) => Effect[Event, State] =
    context => {
      (state, cmd) =>
        context.log.info(s"Got command: $cmd")
        cmd.toEffect(state)
    }

  val eventHandler: (State, Event) => State = { (state, event) =>
    event match {
      case NewGameCreated(_, players) =>
        state.copy(players = players, games = Array.empty)
      case PlayerAdded(_, player, number) =>
        val oldGames =
          if (state.games.nonEmpty) state.games.init else Array.empty[Draw]
        val currentGame =
          if (state.games.nonEmpty) state.games.last else Draw(Seq.empty)
        state.copy(
          games = oldGames :+ Draw(
            currentGame.hands :+ (player, number),
            Seq.empty
          )
        )
      case Solved(_, winners) =>
        val oldGames = state.games.init
        state.copy(
          games = oldGames :+ Draw(state.games.last.hands, winners)
        )
    }
  }

}
