package com.onairentertainment.core

case class CoreException(message: String) extends Throwable(message) {}

object CoreException {
  val NOT_ENOUGH_PLAYERS = "NOT_ENOUGH_PLAYERS"
  val PLAYER_IS_MISSING = "PLAYER_IS_MISSING"
  val INVALID_PLAYER_ID = "INVALID_PLAYER_ID"
}
