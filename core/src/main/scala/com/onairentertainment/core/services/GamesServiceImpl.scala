package com.onairentertainment.core.services

import akka.actor.typed.ActorSystem
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity, EntityRef}
import akka.pattern.StatusReply
import akka.persistence.typed.PersistenceId
import akka.util.Timeout
import com.onairentertainment.core.CoreException
import com.onairentertainment.core.persistence.game.behaviours.GameRoom

import java.util.UUID
import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.concurrent.duration.DurationInt

class GamesServiceImpl(system: ActorSystem[_]) {
  implicit val ec: ExecutionContextExecutor = system.executionContext
  private val sharding = ClusterSharding(system)

  sharding.init(Entity(typeKey = GameRoom.typeKey) { entityContext =>
    GameRoom(
      UUID.fromString(entityContext.entityId),
      PersistenceId(entityContext.entityTypeKey.name, entityContext.entityId)
    )
  })

  implicit val askTimeout: Timeout = Timeout(5.seconds)

  def !(id: UUID, cmd: GameRoom.Command): Unit =
    selectEntity(id) ! cmd

  def selectEntity(id: UUID): EntityRef[GameRoom.Command] = {
    sharding.entityRefFor(GameRoom.typeKey, id.toString)
  }

  def createNewGame(players: Int): Future[GameRoom.State] = {
    selectEntity(UUID.randomUUID())
      .ask(ref => GameRoom.NewGame(players, ref))
      .map {
        case StatusReply.Success(state) => state.asInstanceOf[GameRoom.State]
        case StatusReply.Error(error)   => throw CoreException(error.getMessage)
      }
  }

  def getGame(id: UUID): Future[GameRoom.State] = ???
}

trait GamesService {
  def gamesService: GamesServiceImpl
}
