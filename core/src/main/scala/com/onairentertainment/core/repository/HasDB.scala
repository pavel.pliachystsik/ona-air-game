package com.onairentertainment.core.repository

import slick.jdbc.JdbcBackend

trait HasDB {

  def db: JdbcBackend.Database
}
