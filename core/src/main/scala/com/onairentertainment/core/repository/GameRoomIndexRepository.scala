package com.onairentertainment.core.repository

import com.onairentertainment.core.indexes.models.GameRoomIndexEntry
import slick.lifted.{ProvenShape, Tag}
import slick.jdbc.PostgresProfile.api._

import java.util.UUID

class GameRoomIndexRepository(tag: Tag)
    extends Table[GameRoomIndexEntry](tag, "game_room_index") {

  lazy val id: Rep[UUID] = column[UUID]("id", O.PrimaryKey)
  lazy val players: Rep[Int] = column[Int]("players")

  def * : ProvenShape[GameRoomIndexEntry] = (
    id,
    players
  ) <> ((GameRoomIndexEntry.apply _).tupled, GameRoomIndexEntry.unapply)
}

object GameRoomIndexRepository
    extends TableQuery(new GameRoomIndexRepository(_)) {}
