package com.onairentertainment.core

import com.onairentertainment.core.CoreException.{
  NOT_ENOUGH_PLAYERS,
  PLAYER_IS_MISSING,
  INVALID_PLAYER_ID
}

import scala.annotation.tailrec
import scala.collection.MapView

object GameRules {

  def solve(input: Seq[(Int, Int)]): Seq[(Int, Int)] = {
    val countEachDigit: Seq[Int] => MapView[Int, Int] = (number: Seq[Int]) =>
      number.groupBy(identity).view.mapValues(_.length)

    val produceResult: MapView[Int, Int] => Int =
      (numbers: MapView[Int, Int]) =>
        numbers.foldLeft(0) { case (result, (number, amount)) =>
          (result + number * Math.pow(10, amount - 1)).toInt
        }

    @tailrec
    def toSeq(rest: Int, result: Seq[Int] = Seq.empty): Seq[Int] = {
      if (rest == 0 && result.isEmpty) Seq(0)
      if (rest > 0)
        toSeq(rest / 10, result :+ rest % 10)
      else result
    }

    if (input.exists(_._1 < 0))
      throw CoreException(INVALID_PLAYER_ID)

    if (input.length <= 1)
      throw CoreException(NOT_ENOUGH_PLAYERS)

    if (input.map(_._1).sorted(Ordering.Int.reverse).reduce(_ - _) != 1)
      throw CoreException(PLAYER_IS_MISSING)

    input
      .map { case (player, number) =>
        (player, produceResult(countEachDigit(toSeq(number))))
      }
      .sortBy(_._2)(Ordering.Int.reverse)

  }

}
