package com.onairentertainment.core

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.cluster.typed.{Cluster, Join, Leave}
import akka.persistence.jdbc.testkit.scaladsl.SchemaUtils
import com.onairentertainment.core.indexes.{GameRoomIndex, GameRoomIndexImpl}
import com.onairentertainment.core.repository.{GameRoomIndexRepository, HasDB}
import com.onairentertainment.core.services.{GamesService, GamesServiceImpl}
import slick.jdbc.JdbcBackend.Database

import scala.concurrent.ExecutionContext

object CoreServices extends GamesService with GameRoomIndex with HasDB {
  implicit val system: ActorSystem[Nothing] =
    ActorSystem(Behaviors.empty, "on-air-game-core")

  implicit val ec: ExecutionContext = system.executionContext

  val cluster: Cluster = Cluster(system)
  cluster.manager ! Join(cluster.selfMember.address)

  val gamesService = new GamesServiceImpl(system)

  val db = Database.forConfig("slick.db")

  val gameRoomIndex = new GameRoomIndexImpl(system, db)

  def leaveCluster(): Unit = {
    cluster.manager ! Leave(CoreServices.cluster.selfMember.address)
  }
}
