package unit

import com.onairentertainment.core.{CoreException, GameRules}

class GameRulesUnitTest extends BaseUnitTest {

  "GameRules" must {
    "Solve valid games" must {
      "example game 1" in {
        val game = Seq(
          0 -> 123,
          1 -> 92283
        )
        GameRules.solve(game) mustBe Seq(
          1 -> 40,
          0 -> 6
        )
      }
      "example game 2" in {
        val game = Seq(
          0 -> 9192998,
          1 -> 92283
        )
        GameRules.solve(game) mustBe Seq(
          0 -> 9011,
          1 -> 40
        )
      }

      "example game 3 equal result" in {
        val game = Seq(
          0 -> 9192998,
          1 -> 9192998
        )
        GameRules.solve(game) mustBe Seq(
          0 -> 9011,
          1 -> 9011
        )
      }
      "exaple with 0" in {
        val game = Seq(0 -> 1, 1 -> 2)
        GameRules.solve(game) mustBe Seq(
          1 -> 2,
          0 -> 1
        )
      }
    }
    "Break invalid games" should {
      "Not enough players" in {
        val game = Seq(0 -> 0)
        intercept[CoreException](
          GameRules.solve(game)
        ).message mustBe CoreException.NOT_ENOUGH_PLAYERS
      }
      "Missing player" in {
        val game = Seq(0 -> 0, 2 -> 0)
        intercept[CoreException](
          GameRules.solve(game)
        ).message mustBe CoreException.PLAYER_IS_MISSING
      }
      "Invalid id" in {
        val game = Seq(-1 -> 0)
        intercept[CoreException](
          GameRules.solve(game)
        ).message mustBe CoreException.INVALID_PLAYER_ID
      }
    }
  }

}
