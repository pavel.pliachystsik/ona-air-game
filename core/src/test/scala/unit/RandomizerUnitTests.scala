package unit

import com.onairentertainment.core.services.Randomizer

class RandomizerUnitTests extends BaseUnitTest {
  "Randomizer" must {
    "gives number from 0 to 100000" in {
      val random = Randomizer.getNext
      random mustBe >=(0)
      random mustBe <(100000)
    }
  }
}
