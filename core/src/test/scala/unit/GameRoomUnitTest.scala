package unit

import akka.actor.testkit.typed.scaladsl.{
  ActorTestKitBase,
  ScalaTestWithActorTestKit
}
import akka.pattern.StatusReply
import akka.persistence.typed.PersistenceId
import com.onairentertainment.core.persistence.game.behaviours.GameRoom
import com.onairentertainment.core.persistence.game.behaviours.GameRoom.Draw
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

import java.util.UUID

class GameRoomUnitTest
    extends ActorTestKitBase(s"""
      akka.persistence.journal.plugin = "akka.persistence.journal.inmem"
      akka.persistence.snapshot-store.plugin = "akka.persistence.snapshot-store.local"
      akka.persistence.snapshot-store.local.dir = "target/snapshot-${UUID
      .randomUUID()
      .toString}"
    """)
    with AnyWordSpecLike
    with BeforeAndAfterAll
    with Matchers {
  "Game room" should {
    "New game" should {
      "been empty when is created" in {
        val id = UUID.randomUUID()
        val room = testKit.spawn(
          GameRoom(id, PersistenceId("gameRoom", id.toString)).behavior
        )
        val probe = testKit.createTestProbe[StatusReply[GameRoom.State]]
        room ! GameRoom.NewGame(5, probe.ref)
        val response = probe.receiveMessage()
        response.isSuccess mustBe true
        response.getValue.id mustBe id
        response.getValue.players mustBe 5
      }
      "cannot have less than 2 players" in {
        val id = UUID.randomUUID()
        val room = testKit.spawn(
          GameRoom(id, PersistenceId("gameRoom", id.toString)).behavior
        )
        val probe = testKit.createTestProbe[StatusReply[GameRoom.State]]
        room ! GameRoom.NewGame(1, probe.ref)
        val response = probe.receiveMessage()
        response.isError mustBe true
        response.getError.getMessage mustBe "NOT_ENOUGH_PLAYERS"
      }
      "cannot create game when it have already players" in {
        val id = UUID.randomUUID()
        val room = testKit.spawn(
          GameRoom(id, PersistenceId("gameRoom", id.toString)).behavior
        )
        val probe = testKit.createTestProbe[StatusReply[GameRoom.State]]
        room ! GameRoom.NewGame(2, probe.ref)
        probe.receiveMessage()
        room ! GameRoom.AddPlayer(0, 1, probe.ref)
        probe.receiveMessage()
        room ! GameRoom.NewGame(2, probe.ref)
        val response = probe.receiveMessage()
        response.isError mustBe true
        response.getError.getMessage mustBe "GAME_ALREADY_CREATED"
      }
    }
  }
  "AddPlayer" should {
    "Add player in normal case" in {
      val id = UUID.randomUUID()
      val room = testKit.spawn(
        GameRoom(id, PersistenceId("gameRoom", id.toString)).behavior
      )
      val probe = testKit.createTestProbe[StatusReply[GameRoom.State]]
      room ! GameRoom.NewGame(2, probe.ref)
      probe.receiveMessage()
      room ! GameRoom.AddPlayer(0, 1, probe.ref)
      val response = probe.receiveMessage()
      response.isSuccess mustBe true
      response.getValue.games.head mustBe Draw(Seq((0, 1)))
    }
    "Adding player second time will response error" in {
      val id = UUID.randomUUID()
      val room = testKit.spawn(
        GameRoom(id, PersistenceId("gameRoom", id.toString)).behavior
      )
      val probe = testKit.createTestProbe[StatusReply[GameRoom.State]]
      room ! GameRoom.NewGame(2, probe.ref)
      probe.receiveMessage()
      room ! GameRoom.AddPlayer(0, 1, probe.ref)
      probe.receiveMessage()
      room ! GameRoom.AddPlayer(0, 1, probe.ref)
      val response = probe.receiveMessage()
      response.isError mustBe true
      response.getError.getMessage mustBe "PLAYER_PLAYED"
    }
    "Adding player which may not exist response error" in {
      val id = UUID.randomUUID()
      val room = testKit.spawn(
        GameRoom(id, PersistenceId("gameRoom", id.toString)).behavior
      )
      val probe = testKit.createTestProbe[StatusReply[GameRoom.State]]
      room ! GameRoom.NewGame(2, probe.ref)
      probe.receiveMessage()
      room ! GameRoom.AddPlayer(10, 1, probe.ref)
      val response = probe.receiveMessage()
      response.isError mustBe true
      response.getError.getMessage mustBe "PLAYER_NOT_EXISTS"
    }
    "Adding last player wil solve game" in {
      val id = UUID.randomUUID()
      val room = testKit.spawn(
        GameRoom(id, PersistenceId("gameRoom", id.toString)).behavior
      )
      val probe = testKit.createTestProbe[StatusReply[GameRoom.State]]
      room ! GameRoom.NewGame(2, probe.ref)
      probe.receiveMessage()
      room ! GameRoom.AddPlayer(0, 1, probe.ref)
      probe.receiveMessage()
      room ! GameRoom.AddPlayer(1, 2, probe.ref)
      val response = probe.receiveMessage()
      response.isSuccess mustBe true
      response.getValue.games.head.winner mustBe Seq(1)
    }
  }
}
