package unit

import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

trait BaseUnitTest extends AnyWordSpecLike with Matchers {}
