import Dependencies._

ThisBuild / name := "onair-game"
ThisBuild / organization := "com.onairentertainment"
ThisBuild / version := "0.1"

scalaVersion := "2.13.6"

lazy val coreModule = project
  .in(file("core"))
  .settings(
    libraryDependencies ++= Dependencies.Specs.deps ++ Akka.typed ++ Seq(
      Common.logback,
      Dependencies.SQL.postgreSQL
    )
  )

lazy val root = Project(id = "server", base = file("."))
  .dependsOn(coreModule % "compile;test->test")

libraryDependencies ++= Akka.typed ++ Akka.http ++ Circe.deps ++ Seq(
  Akka.akkaTestkit,
  Akka.streamTestkit,
  Akka.akkaHttpTestkit,
  Specs.scalaTest % Test,
  Common.logback
)
